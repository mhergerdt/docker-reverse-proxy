#!/usr/bin/env bash

DOMAIN=$1
COMPANY=$2

if [ -z "$DOMAIN" ]; then
  echo -n 'Enter root domain (no www): '
  read input_d
  DOMAIN=$input_d
fi

if [ -z "$COMPANY" ]; then
  echo -n 'Enter company name: '
  read input_d
  COMPANY=$input_d
fi

[ -d certs ] || mkdir certs

# Easiest to generate conf file for each
# certificate creation process
OpenSSLSubj="/C=DE/ST=Berlin/L=Berlin/O=$COMPANY/OU=Develpment/CN=$COMPANY"
OpenSSLExtFile="$DOMAIN"-v3.ext

cat >"$OpenSSLExtFile" <<EOL
authorityKeyIdentifier = keyid,issuer
basicConstraints = CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $DOMAIN
DNS.2 = *.$DOMAIN
EOL

# Create Private RSA Key
echo 'Create Private RSA Key:'
openssl genrsa -out "certs/root_ca_$DOMAIN".key 2048

# Generate Root Certificate
echo 'Generate Root Certificate:'
openssl req -x509 -new -nodes -key "certs/root_ca_$DOMAIN".key -sha256 \
-days 365 -out "certs/root_ca_$DOMAIN".pem -subj "$OpenSSLSubj"

# Create a private key for the SSL certificate and a certificate signing request
echo 'Create a private key for the SSL certificate and a certificate signing request:'
openssl req -new -nodes -out "certs/$DOMAIN".csr -newkey rsa:2048 \
-keyout "certs/$DOMAIN".key -subj "$OpenSSLSubj"

# Issue the certificate
echo 'Issue the certificate:'
openssl x509 -req -in "certs/$DOMAIN".csr -CA "certs/root_ca_$DOMAIN".pem \
-CAkey "certs/root_ca_$DOMAIN".key -CAcreateserial -out "certs/$DOMAIN".crt \
-days 365 -sha256 -extfile "$OpenSSLExtFile"

# Remove temp files
echo 'Remove temp files:'
rm -- "$OpenSSLExtFile"
