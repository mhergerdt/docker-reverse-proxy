# Create a self signed wildcard certificate
Run the script "generate-wildcard-certificate.sh"

# Import the root ca file

### Chrome
Open the settings page, scroll down to the end and click on "advanced" to show more options. Scroll down and click "Manage certificates".
For Linux Users:
Select the tab "Authorities" and click on "Import". This will open a file chooser where you need to change the shown file types to all.
Now navigate to the directory containing the generated files and open the root_ca_YOURDOMAIN.TLD.pem file.
Check the "Trust this certificate to identifying websites and click "ok"

### Firefox
@TODO